//
//  BonkerBallzAppDelegate.h
//  BonkerBallz
//
//  Created by Winfield Hill on 3/14/11.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BonkerBallzViewController;

@interface BonkerBallzAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    BonkerBallzViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet BonkerBallzViewController *viewController;

@end

