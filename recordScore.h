//
//  recordScore.h
//  Bonker Ballz
//
//  Created by Winfield Hill on 4/18/11.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BonkerBallzViewController;

@interface recordScore : NSObject {
	int recordIntScore;
}

- (void)setScore:(int)s;

@end
