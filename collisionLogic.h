//
//  collisionLogic.h
//  BonkerBallz
//
//  Created by Winfield Hill on 3/31/11.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface collisionLogic : NSObject {
    SystemSoundID beepSound;
    SystemSoundID bbeepSound;
}

@property(nonatomic, assign) SystemSoundID beepSound;
@property(nonatomic, assign) SystemSoundID bbeepSound;
-(void)loadSound;
-(void)playSound;
-(void)loadSoundb;
-(void)playSoundb;

@end
