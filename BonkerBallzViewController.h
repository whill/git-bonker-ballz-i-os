//
//  BonkerBallzViewController.h
//  BonkerBallz
//
//  Created by Winfield Hill on 3/14/11.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CADisplayLink.h>
@class collisionLogic;

@interface BonkerBallzViewController : UIViewController<UIAccelerometerDelegate> {
	IBOutlet UILabel *scoreLabel;
	int score;
    int goalScore;
    IBOutlet UILabel *goalScoreLabel;
    int timeleft;
    IBOutlet UILabel *timeLeftLabel;
    UIImageView *backgroundImage[10];
    NSString *backgroundImageNames[10];
	
	IBOutlet UIImageView *ball;
	UIImageView *ball2;
	CGPoint ballMovement;
	CGPoint ballMovement2;
	
	IBOutlet UIImageView *paddle;
	IBOutlet UIImageView *paddleLSide;
	IBOutlet UIImageView *paddleRSide;
	
	BOOL paddleCollision;
	BOOL paddleLSideCollision;
	BOOL paddleRSideCollision;
	BOOL paddleCollision2;
	BOOL paddleLSideCollision2;
	BOOL paddleRSideCollision2;
	
	int lives;
	IBOutlet UILabel *livesLabel;
	IBOutlet UILabel *messageLabel;
	IBOutlet UILabel *startGameLabel;
	
    BOOL calibrationMode;
	BOOL isPlaying;
	NSTimer *theLevelTimer;
	CADisplayLink *displayLink;
    int sTimer;
    int bBallTimer;
	
	UIButton *easyButton;
	UIButton *mediumButton;
	UIButton *extremeButton;
	IBOutlet UIImageView *easyButtonGlow;
	IBOutlet UIImageView *mediumButtonGlow;
	IBOutlet UIImageView *extremeButtonGlow;
	
	BOOL demoMode;
	NSString *difficultyLevel;
	UIAccelerometer *theAccel;
    float accelXOffset, accelYOffset;
	
	collisionLogic *collisionPrint;
}

@property(nonatomic, retain)IBOutlet UIButton *easyButton;
@property(nonatomic, retain)IBOutlet UIButton *mediumButton;
@property(nonatomic, retain)IBOutlet UIButton *extremeButton;
@property(nonatomic) float accelXOffset;

-(void)startDemo;
-(void)startPlaying;
-(void)pauseGame;
-(void)initalizeTimer;
-(void)initalizeLevelTimer;
-(void)levelLogic:(NSTimer *)theLevelTimer;
-(void)motionLogic:(CADisplayLink *)displayLink;
-(void)easyStart;
-(void)mediumStart;
-(void)extremeStart;
-(void)captureDeviceOrientation:(float)accelX verticalMovement:(float)accelY;

@end

