//
//  bonkerBallz.m
//  BonkerBallz
//
//  Created by Winfield Hill on 3/14/11.
//  Copyright 2011 Home. All rights reserved.
//

#import "BonkerBallzViewController.h"
#import "collisionLogic.h"

@implementation BonkerBallzViewController
@synthesize easyButton, mediumButton, extremeButton, accelXOffset;


- (void)dealloc {
	[scoreLabel release];
    [goalScoreLabel release];
    [timeLeftLabel release];
	[ball release];
	[paddle release];
	[livesLabel release];
	[messageLabel release];
	[paddleLSide release];
	[paddleRSide release];
    [displayLink release];
    [theLevelTimer release];
    [super dealloc];
}

- (void)viewDidUnload {
	[scoreLabel release];
        scoreLabel=nil;
    [goalScoreLabel release];
        goalScoreLabel = nil;
    [timeLeftLabel release];
        timeLeftLabel = nil;
	[ball release];
        ball=nil;
	if (ball2) {
		[ball2 release];
            ball2=nil;
	}
	[paddle release];
        paddle=nil;
	[livesLabel release];
        livesLabel=nil;
	[messageLabel release];
        messageLabel=nil;
	[paddleLSide release];
        paddleLSide=nil;
	[paddleRSide release];
        paddleRSide=nil;
    [displayLink release];
        displayLink = nil;
    [theLevelTimer release];
        theLevelTimer = nil;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    sTimer=1;
    backgroundImageNames[0] = @"space1.png";
    backgroundImageNames[1] = @"space2.png";
    backgroundImageNames[2] = @"space3.png";
    backgroundImageNames[3] = @"space4.png";
    backgroundImageNames[4] = @"space5.png";
    backgroundImageNames[5] = @"space6.png";
    backgroundImageNames[6] = @"space7.png";
    backgroundImageNames[7] = @"space8.png";
    backgroundImageNames[8] = @"space9.png";
    backgroundImageNames[9] = @"space10.png";
    
    for (int i=0; i<10; i++) {
        UIImage *image = [UIImage imageNamed:backgroundImageNames[i]];
        backgroundImage[i] = [[UIImageView alloc] initWithImage:image];
        backgroundImage[i].frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    }
    
    calibrationMode = FALSE;
	demoMode = TRUE;
    [super viewDidLoad];
	[self startDemo];
	
	/*if (!collisionPrint) {
		collisionPrint = [[collisionLogic alloc] init];
	}*/
	
	[easyButton addTarget:self action:@selector(easyStart) forControlEvents:UIControlEventTouchUpInside];
	[mediumButton addTarget:self action:@selector(mediumStart) forControlEvents:UIControlEventTouchUpInside];
	[extremeButton addTarget:self action:@selector(extremeStart) forControlEvents:UIControlEventTouchUpInside];
	
	/*CGRect startGameLabelRect = CGRectMake(0, 255, [[UIScreen mainScreen] bounds].size.width, 30);
	startGameLabel = [[UILabel alloc] initWithFrame:startGameLabelRect];
	startGameLabel.textAlignment = UITextAlignmentCenter;
	startGameLabel.textColor = [UIColor whiteColor];
	startGameLabel.backgroundColor = [UIColor clearColor];
	//startGameLabel.font = [UIFont fontWithName:@"Courier" size:25];
	startGameLabel.text = @"Tap Screen to Begin!";
	startGameLabel.hidden = TRUE;
	[self.view addSubview:startGameLabel];*/
	/*CGRect fireMessageFrame;
	 fireMessageFrame.origin = CGPointMake(0, 220);
	 fireMessageFrame.size = CGSizeMake(330, 25);
	 fireMessage = [[UILabel alloc] initWithFrame:fireMessageFrame];
	 fireMessage.text = @"You're on FIRE!!!";
	 fireMessage.font = [UIFont fontWithName:@"arial" size:25];
	 fireMessage.textColor = [UIColor redColor];
	 fireMessage.textAlignment = UITextAlignmentCenter;
	 fireMessage.backgroundColor = [UIColor clearColor];
	 fireMessage.alpha = 0.0;
	 fireMessage.hidden = TRUE;
	 [self.view addSubview:fireMessage];*/
    
    collisionPrint = [[collisionLogic alloc] init];
    [collisionPrint loadSound];
}

-(void)easyStart{
	[displayLink invalidate];
	displayLink = nil;
	
	lives=5;
	score=0;
    goalScore=250;
    timeleft=120;
    sTimer=1;
    bBallTimer=1;
	ballMovement = CGPointMake(3,3);
	
	isPlaying = TRUE;
	demoMode = FALSE;
	difficultyLevel = @"easyLevel";
	[self startPlaying];
	startGameLabel.hidden = FALSE;
	
	easyButton.hidden = TRUE;
	easyButtonGlow.hidden = TRUE;
	mediumButton.hidden = TRUE;
	mediumButtonGlow.hidden = TRUE;
	extremeButton.hidden = TRUE;
	extremeButtonGlow.hidden = TRUE;
	if (!theAccel) {
		theAccel = [UIAccelerometer sharedAccelerometer];
		theAccel.updateInterval = 1.0/60.0;
		theAccel.delegate = self;
	}
}
-(void)mediumStart{
	[displayLink invalidate];
	displayLink = nil;

	lives=3;
	score=0;
    goalScore=300;
    timeleft=90;
    sTimer=1;
    bBallTimer=1;
	ballMovement = CGPointMake(4,4);
	
	isPlaying = TRUE;
	demoMode = FALSE;
	difficultyLevel = @"mediumLevel";
	[self startPlaying];
	startGameLabel.hidden = FALSE;
	
	easyButton.hidden = TRUE;
	easyButtonGlow.hidden = TRUE;
	mediumButton.hidden = TRUE;
	mediumButtonGlow.hidden = TRUE;
	extremeButton.hidden = TRUE;
	extremeButtonGlow.hidden = TRUE;
	if (!theAccel) {
		theAccel = [UIAccelerometer sharedAccelerometer];
		theAccel.updateInterval = 1.0/60.0;
		theAccel.delegate = self;
	}
}
-(void)extremeStart{
	[displayLink invalidate];
	displayLink = nil;

	lives=2;
	score=0;
    goalScore=350;
    timeleft=60;
    sTimer=1;
    bBallTimer=1;
	ballMovement = CGPointMake(5,5);
	
	isPlaying = TRUE;
	demoMode = FALSE;
	difficultyLevel = @"extremeLevel";
	[self startPlaying];
	startGameLabel.hidden = FALSE;
	
	easyButton.hidden = TRUE;
	easyButtonGlow.hidden = TRUE;
	mediumButton.hidden = TRUE;
	mediumButtonGlow.hidden = TRUE;
	extremeButton.hidden = TRUE;
	extremeButtonGlow.hidden = TRUE;
	if (!theAccel) {
		theAccel = [UIAccelerometer sharedAccelerometer];
		theAccel.updateInterval = 1.0/60.0;
		theAccel.delegate = self;
	}
}

-(void)startDemo{
	score=0;
    goalScore=450;
    timeleft=60;
    sTimer=1;
    bBallTimer=1;
	if (!lives) {
        if (difficultyLevel == @"extremeLevel") {
            lives = 2;
            ballMovement = CGPointMake(5, 5);
        }else if(difficultyLevel == @"mediumLevel"){
            lives = 3;
            ballMovement = CGPointMake(4, 4);
        }else{
            lives=5;
            ballMovement = CGPointMake(3, 3);
        }
	}
	scoreLabel.text = [NSString stringWithFormat:@"%i", score];
	scoreLabel.textColor = [UIColor whiteColor];
	livesLabel.text = [NSString stringWithFormat:@"%i", lives];
    goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
    timeLeftLabel.text = [NSString stringWithFormat:@"%i", timeleft];
    
	ball.center = CGPointMake(159, 230);
	paddle.center = CGPointMake(159, 399);
	
	messageLabel.hidden = TRUE;
	
	[self initalizeTimer];
    [self initalizeLevelTimer];
	isPlaying = FALSE;
}
-(void)startPlaying{
	if (!lives) {
		lives=3;
		score=0;
	}
    if(ball2){
        ball2.hidden = TRUE;
        [ball2 release];
        ball2 = nil;
    }
	scoreLabel.text = [NSString stringWithFormat:@"%i", score];
	scoreLabel.textColor = [UIColor whiteColor];
	livesLabel.text = [NSString stringWithFormat:@"%i", lives];
    goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
    timeLeftLabel.text = [NSString stringWithFormat:@"%i", timeleft];
	
	//ballMovement = CGPointMake(3, 3);
	ball.center = CGPointMake(159, 230);
	paddle.center = CGPointMake(159, 399);

	messageLabel.hidden = TRUE;
	
	//[self initalizeTimer];
	isPlaying = TRUE;
}

-(void)pauseGame{
	[displayLink invalidate];
	displayLink = nil;
    [theLevelTimer invalidate];
    theLevelTimer = nil;
	ball.center = CGPointMake(159, 100);
	paddle.center = CGPointMake(159, 399);
	paddleLSide.center = CGPointMake(15, 230);
	paddleRSide.center = CGPointMake(305, 230);
}

-(void)initalizeTimer{
	if (!displayLink) {
		displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(motionLogic:)];
		[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	}
}
-(void)initalizeLevelTimer{
	if (theLevelTimer == nil) {
        float theInterval = 1.0;
        theLevelTimer = [NSTimer scheduledTimerWithTimeInterval:theInterval target:self selector:@selector(levelLogic:) userInfo:nil repeats:TRUE];
     }
}

-(void)levelLogic:(NSTimer *)theLevelTimer{
    //NSLog(@"score percentage divisor result=%i", sTimer%100);
    //NSLog(@"score=%i", sTimer);
    sTimer+=1;
    timeleft -= 1;
    timeLeftLabel.text = [NSString stringWithFormat:@"%i", timeleft];
    if (timeleft==0 && score<goalScore) {
		[self pauseGame];
		isPlaying = FALSE;
		lives--;
		livesLabel.text = [NSString stringWithFormat:@"%i", lives];
        
        
		if(!lives){
			messageLabel.text=@"Game Over!";
            demoMode = TRUE;
            [self startDemo];
            score = 0;
            if (ball2) {
                ball2.hidden = TRUE;
                [ball2 release];
                ball2 = nil;
                bBallTimer = 1;	
            }
		}else {
			messageLabel.text=@"Out of Time!";
            if (difficultyLevel==@"easyLevel"){
                timeleft=120;
            }else if(difficultyLevel==@"mediumLevel"){
                timeleft=90;
            }else if(difficultyLevel==@"extremeLevel"){
                timeleft=60;
            }
		}
		messageLabel.hidden = FALSE;
		
		ball2.hidden = TRUE;
		[ball2 release];
		ball2 = nil;
        bBallTimer = 1;
    }else if(timeleft==0 && score>=goalScore){
        bBallTimer+=1;
        if (bBallTimer%50==0) {
            if (!ball2) {
                ball2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ball2.png"]];
                ball2.center = CGPointMake(159,50);
                if (difficultyLevel==@"easyLevel") {
                    ballMovement2 = CGPointMake(3,3);
                }else if (difficultyLevel==@"mediumLevel") {
                    ballMovement2 = CGPointMake(4,4);
                }else if (difficultyLevel==@"extremeLevel") {
                    ballMovement2 = CGPointMake(5,5);
                }else if(demoMode)
                    ballMovement2 = CGPointMake(3,3);
            
                [self.view insertSubview:ball2 atIndex:20];
                bBallTimer=1;
            }
        }
        /* The level incrementation has to be based on the level setting... doesn't make sense to advance from level 1 to level 2 for easy mode before user has reached their target score.
        if(difficultyLevel==@"easyLevel"){
            
        }else if(difficultyLevel==@"mediumLevel"){
            
            
        }else if(difficultyLevel==@"extremeLevel"){
            
        }else if(demoMode){
            
        }*/
        if (sTimer>=60 && sTimer<120) {
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 500;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 600;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 700;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 1000;}
            goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
            [self.view insertSubview:backgroundImage[1] atIndex:1];
        }else if(sTimer>=120 && sTimer<180){
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 750;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 900;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 1050;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 1500;}
            goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
            [self.view insertSubview:backgroundImage[2] aboveSubview:backgroundImage[1]];
        }else if(sTimer>=180 && sTimer<240){
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 1000;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 1200;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 1400;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 2000;}
            goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
            [self.view insertSubview:backgroundImage[3] aboveSubview:backgroundImage[2]];
        }
        else if(sTimer>=240 && sTimer<300){
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 1250;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 1500;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 1750;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 2500;}
            goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
            [self.view insertSubview:backgroundImage[4] aboveSubview:backgroundImage[3]];
        }else if(sTimer>=300 && sTimer<360){
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 1500;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 1800;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 2100;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 3000;}
            goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
            [self.view insertSubview:backgroundImage[5] aboveSubview:backgroundImage[4]];
        }else if(sTimer>=360 && sTimer<420){
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 1750;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 2100;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 2450;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 3500;}
            goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
            [self.view insertSubview:backgroundImage[6] aboveSubview:backgroundImage[5]];
        }else if(sTimer>=420 && sTimer<480){
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 2000;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 2400;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 2800;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 4000;}
                goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
                [self.view insertSubview:backgroundImage[7] aboveSubview:backgroundImage[6]];
        }else if(sTimer>=480 && sTimer<540){
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 2250;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 2700;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 3150;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 4500;}
                goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
                [self.view insertSubview:backgroundImage[8] aboveSubview:backgroundImage[7]];
        }else if(sTimer>=540){
            if (difficultyLevel==@"easyLevel") {
                timeleft = 120;
                goalScore = 2500;
            }else if (difficultyLevel==@"mediumLevel") {
                timeleft = 90;
                goalScore = 3000;
            }else if (difficultyLevel==@"extremeLevel") {
                timeleft = 60;
                goalScore = 3500;
            }else if(demoMode){
                timeleft = 60;
                goalScore = 5000;}
                goalScoreLabel.text = [NSString stringWithFormat:@"%i", goalScore];
                [self.view insertSubview:backgroundImage[9] aboveSubview:backgroundImage[8]];
        }
    }
}
-(void)motionLogic:(CADisplayLink *)displayLink{
	if (demoMode) {
		paddle.center = CGPointMake(ball.center.x, paddle.center.y);
		paddleLSide.center = CGPointMake(paddleLSide.center.x, ball.center.y);
		paddleRSide.center =  CGPointMake(paddleRSide.center.x, ball.center.y);
	}
	//[collisionPrint printTest];
	ball.center = CGPointMake(ball.center.x+ballMovement.x, ball.center.y+ballMovement.y);
	
	paddleCollision =
		ball.center.y >= paddle.center.y - 16 && 
		ball.center.y <= paddle.center.y +16 &&
		ball.center.x > paddle.center.x-32 &&
		ball.center.x < paddle.center.x+32;
	paddleLSideCollision =
		ball.center.y >= paddleLSide.center.y - 32 &&
		ball.center.y <= paddleLSide.center.y+32 &&
		ball.center.x > paddleLSide.center.x-16 &&
		ball.center.x < paddleLSide.center.x+16;
	paddleRSideCollision =
		ball.center.y >= paddleRSide.center.y - 32 &&
		ball.center.y <= paddleRSide.center.y+32 &&
		ball.center.x > paddleRSide.center.x-16 &&
		ball.center.x < paddleRSide.center.x+16;
	
	if(paddleCollision){
        [collisionPrint playSound];
		ballMovement.y = -ballMovement.y;
		
		if (ball.center.y >= paddle.center.y - 16 && ballMovement.y < 0) {
			ball.center = CGPointMake(ball.center.x, paddle.center.y-16);
		}else if (ball.center.y<paddle.center.y+16 && ballMovement.y>0) {
			ball.center = CGPointMake(ball.center.x, paddle.center.y+16);
		}
		score+=5;
		scoreLabel.text = [NSString stringWithFormat:@"%i", score];
	}else if (paddleLSideCollision){
        [collisionPrint playSound];
		ballMovement.x = -ballMovement.x;
		score+=10;
		scoreLabel.text = [NSString stringWithFormat:@"%i", score];
		
		if(ball.center.x >= paddleLSide.center.x - 16 && ballMovement.x < 0){
			ball.center = CGPointMake(paddleLSide.center.x+16,ball.center.y);
		}else if (ball.center.x < paddleLSide.center.x+16 && ballMovement.x>0) {
			ball.center = CGPointMake(paddleLSide.center.x+16, ball.center.y);
		}
	}else if (paddleRSideCollision){
        [collisionPrint playSound];
		ballMovement.x = -ballMovement.x;
		score+=10;
		scoreLabel.text = [NSString stringWithFormat:@"%i", score];
		
		if(ball.center.x >= paddleRSide.center.x - 16 && ballMovement.x < 0){
			ball.center = CGPointMake(paddleRSide.center.x-16,ball.center.y);
		}else if (ball.center.x < paddleLSide.center.x+16 && ballMovement.x>0) {
			ball.center = CGPointMake(paddleLSide.center.x+16, ball.center.y);
		}
	}
	if (ball2) {
		paddleCollision2 =
			ball2.center.y >= paddle.center.y - 16 && 
			ball2.center.y <= paddle.center.y +16 &&
			ball2.center.x > paddle.center.x-32 &&
			ball2.center.x < paddle.center.x+32;
		paddleLSideCollision2 =
			ball2.center.y >= paddleLSide.center.y - 32 &&
			ball2.center.y <= paddleLSide.center.y+32 &&
			ball2.center.x > paddleLSide.center.x-16 &&
			ball2.center.x < paddleLSide.center.x+16;
		paddleRSideCollision2 =
			ball2.center.y >= paddleRSide.center.y - 32 &&
			ball2.center.y <= paddleRSide.center.y+32 &&
			ball2.center.x > paddleRSide.center.x-16 &&
			ball2.center.x < paddleRSide.center.x+16;
	
		if(paddleCollision2){
            [collisionPrint playSound];
			ballMovement2.y = -ballMovement2.y;
		
			if (ball2.center.y >= paddle.center.y - 16 && ballMovement2.y < 0) {
				ball2.center = CGPointMake(ball2.center.x, paddle.center.y-16);
			}else if (ball2.center.y<paddle.center.y+16 && ballMovement2.y>0) {
				ball2.center = CGPointMake(ball2.center.x, paddle.center.y+16);
			}
			score+=5;
			scoreLabel.text = [NSString stringWithFormat:@"%i", score];
		}else if (paddleLSideCollision2){
            [collisionPrint playSound];
			ballMovement2.x = -ballMovement2.x;
			score+=10;
			scoreLabel.text = [NSString stringWithFormat:@"%i", score];
		
			if(ball2.center.x >= paddleLSide.center.x - 16 && ballMovement2.x < 0){
				ball2.center = CGPointMake(paddleLSide.center.x+16,ball2.center.y);
			}else if (ball2.center.x < paddleLSide.center.x+16 && ballMovement2.x>0) {
				ball2.center = CGPointMake(paddleLSide.center.x+16, ball2.center.y);
			}
		}else if (paddleRSideCollision2){
            [collisionPrint playSound];
			ballMovement2.x = -ballMovement2.x;
			score+=10;
			scoreLabel.text = [NSString stringWithFormat:@"%i", score];
		
			if(ball2.center.x >= paddleRSide.center.x - 16 && ballMovement2.x < 0){
				ball2.center = CGPointMake(paddleRSide.center.x-16,ball2.center.y);
			}else if (ball2.center.x < paddleLSide.center.x+16 && ballMovement2.x>0) {
				ball2.center = CGPointMake(paddleLSide.center.x+16, ball2.center.y);
			}
		}
		ball2.center = CGPointMake(ball2.center.x+ballMovement2.x, ball2.center.y+ballMovement2.y);
		if (ball2.center.x>307 || ball2.center.x<14)
			ballMovement2.x=-ballMovement2.x;
		
		if(ball2.center.y<32)
			ballMovement2.y = -ballMovement2.y;
		if (ball2.center.y>444) {
			ball2.hidden = TRUE;
			[ball2 release];
			ball2 = nil;
            
            bBallTimer =1;
		}		
	}
	
	

	if (ball.center.x>310 || ball.center.x<16)
		ballMovement.x=-ballMovement.x;
	
	if(ball.center.y<32)
		ballMovement.y = -ballMovement.y;
		
	if (ball.center.y>444) {
		[self pauseGame];
		isPlaying = FALSE;
		lives--;
		livesLabel.text = [NSString stringWithFormat:@"%i", lives];
        
		if(!lives){
			//messageLabel.text=@"Game Over!";
            demoMode = TRUE;
            [self startDemo];
            difficultyLevel=@"demo";
            score = 0;
            if (ball2) {
                ball2.hidden = TRUE;
                [ball2 release];
                ball2 = nil;
            }
            easyButton.hidden = FALSE;
            easyButtonGlow.hidden = FALSE;
            mediumButton.hidden = FALSE;
            mediumButtonGlow.hidden = FALSE;
            extremeButton.hidden = FALSE;
            extremeButtonGlow.hidden = FALSE;
		}else {
			messageLabel.text=@"Out of bounds!";
		}
		messageLabel.hidden = TRUE;
		
		ball2.hidden = TRUE;
		[ball2 release];
		ball2 = nil;
        bBallTimer = 1;
	}
}

-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)accel{
	if(isPlaying && difficultyLevel!=nil){
		float newX = paddle.center.x + (accel.x * 40);
		if(newX > 30 && newX < 290)
			paddle.center = CGPointMake(newX, paddle.center.y);
		float newY = paddleLSide.center.y + (-accel.y * 40);
		if(newY > 32 && newY < 382){
			paddleLSide.center = CGPointMake(paddleLSide.center.x, newY);
			paddleRSide.center = CGPointMake(paddleRSide.center.x, newY);
		}
	}
    if(calibrationMode){
        [self captureDeviceOrientation:accel.x verticalMovement:accel.y];
    }
}

-(void)captureDeviceOrientation:(float)accelX verticalMovement:(float)accelY{
    accelXOffset = accelX;
    accelYOffset = accelY;
    //NSLog(@"accel in x direction:%f and the accel in the y direction:%f", accelXOffset, accelY);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	if (isPlaying && lives >0) {
        calibrationMode = FALSE;
		startGameLabel.hidden = TRUE;
		[self initalizeTimer];
        [self initalizeLevelTimer];
		if (!theAccel) {
			theAccel = [UIAccelerometer sharedAccelerometer];
			theAccel.updateInterval = 1.0/60.0;
			theAccel.delegate = self;
            //set bBallTimer and sTimer??? here and below...
		}
	}else if (!isPlaying && lives!=0 && difficultyLevel!=nil) {
		[self initalizeTimer];
        [self initalizeLevelTimer];
		messageLabel.hidden = TRUE;
        isPlaying = TRUE;
        demoMode = FALSE;
        calibrationMode = FALSE;
	}else if (!isPlaying && lives==0) {
        [self startPlaying];
        calibrationMode = FALSE;
	}
}


/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

@end
