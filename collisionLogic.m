//
//  collisionLogic.m
//  BonkerBallz
//
//  Created by Winfield Hill on 3/31/11.
//  Copyright 2011 Home. All rights reserved.
//

#import "collisionLogic.h"


@implementation collisionLogic

@synthesize beepSound, bbeepSound;

-(void)loadSound{
	//NSLog(@"Test from collision class.");
    
    NSURL *beepSoundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                                   pathForResource:@"My Song" ofType:@"aif"]];
                           
    
    AudioServicesCreateSystemSoundID((CFURLRef)beepSoundURL, &beepSound);
}

-(void)playSound{
    //NSLog(@"Play sound");
    AudioServicesPlaySystemSound(self.beepSound);
}
-(void)loadSoundb{
	//NSLog(@"Test from collision class.");
    
    NSURL *bbeepSoundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                                  pathForResource:@"bballsound" ofType:@"aif"]];
    
    
    AudioServicesCreateSystemSoundID((CFURLRef)bbeepSoundURL, &bbeepSound);
}

-(void)playSoundb{
    //NSLog(@"Play sound");
    AudioServicesPlaySystemSound(self.bbeepSound);
}

-(void)dealloc{
    AudioServicesDisposeSystemSoundID(beepSound);
    AudioServicesDisposeSystemSoundID(bbeepSound);
    [super dealloc];
}

@end
